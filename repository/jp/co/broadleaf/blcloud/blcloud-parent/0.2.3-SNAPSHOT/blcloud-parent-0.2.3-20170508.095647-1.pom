<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>jp.co.broadleaf.blcloud</groupId>
  <artifactId>blcloud-parent</artifactId>
  <version>0.2.3-SNAPSHOT</version>
  <packaging>pom</packaging>

  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>1.5.1.RELEASE</version>
    <relativePath></relativePath>
  </parent>

  <properties>
    <blcloud.version>0.2.0-SNAPSHOT</blcloud.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <java.version>1.8</java.version>
    <cassandra.version>3.9</cassandra.version>
    <cassandra-driver.version>3.1.3</cassandra-driver.version>
    <checkstyle.location>${basedir}/../blcloud-tools/formatter/checkstyle</checkstyle.location>
    <spring.data.common.version>1.13.0.RELEASE</spring.data.common.version>
    <spring.data.jdbc.core.version>1.2.1.RELEASE</spring.data.jdbc.core.version>
    <com.google.cloud.version>0.9.2-beta</com.google.cloud.version>
    <cglib.version>3.2.4</cglib.version>
    <maven.jxr.plugin.version>2.5</maven.jxr.plugin.version>
    <powermock.module.junit4.version>1.6.6</powermock.module.junit4.version>
    <powermock.api.mockito.version>1.6.6</powermock.api.mockito.version>
    <commons.lang3.version>3.5</commons.lang3.version>
    <maven.javadoc.failOnError>false</maven.javadoc.failOnError>
    <jacoco.version>0.7.9</jacoco.version>
  </properties>

  <dependencies>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-aop</artifactId>
    </dependency>

    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.datatype</groupId>
      <artifactId>jackson-datatype-jsr310</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
    </dependency>

    <!-- https://cloud.google.com/storage/docs/reference/libraries#client-libraries-install-java -->
    <dependency>
      <groupId>com.google.cloud</groupId>
      <artifactId>google-cloud-storage</artifactId>
      <version>${com.google.cloud.version}</version>
      <exclusions>
        <exclusion>
          <groupId>javax.servlet</groupId>
          <artifactId>servlet-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>${commons.lang3.version}</version>
    </dependency>

    <dependency>
      <groupId>org.mockito</groupId>
      <artifactId>mockito-core</artifactId>
      <scope>test</scope>
    </dependency>

    <!-- https://mvnrepository.com/artifact/org.powermock/powermock-api-mockito -->
    <dependency>
      <groupId>org.powermock</groupId>
      <artifactId>powermock-api-mockito</artifactId>
      <version>${powermock.api.mockito.version}</version>
      <scope>test</scope>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.powermock/powermock-module-junit4 -->
    <dependency>
      <groupId>org.powermock</groupId>
      <artifactId>powermock-module-junit4</artifactId>
      <version>${powermock.module.junit4.version}</version>
      <scope>test</scope>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.assertj/assertj-core -->
    <dependency>
      <groupId>org.assertj</groupId>
      <artifactId>assertj-core</artifactId>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-jxr-plugin</artifactId>
      <version>${maven.jxr.plugin.version}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>cglib</groupId>
      <artifactId>cglib</artifactId>
      <version>${cglib.version}</version>
    </dependency>
  </dependencies>

  <build>
    <plugins>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <argLine>${jacocoArgs}</argLine>
          <includes>
            <include>**/*Test.java</include>
          </includes>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>${jacoco.version}</version>
        <executions>
          <execution>
            <id>prepare-agent</id>
            <phase>test-compile</phase>
            <goals>
              <goal>prepare-agent</goal>
            </goals>
            <configuration>
              <includes>
                <include>jp/co/broadleaf/**</include>
              </includes>
              <propertyName>jacocoArgs</propertyName>
              <append>true</append>
            </configuration>
          </execution>
          <execution>
            <id>default-report</id>
            <phase>prepare-package</phase>
            <goals>
              <goal>report</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

    </plugins>
  </build>

  <reporting>
    <plugins>
      <!-- Maven Project Info Reports plugin -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <reportSets>
          <reportSet>
            <reports>
              <report>index</report>
              <report>summary</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>

      <!-- Checkstyle plugin -->
      <plugin>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <configuration>
          <configLocation>${checkstyle.location}/checkstyle.xml</configLocation>
          <config_loc>${checkstyle.location}/checkstyle.xml</config_loc>
          <enableRulesSummary>false</enableRulesSummary>
          <propertyExpansion>config_loc=${checkstyle.location}</propertyExpansion>
        </configuration>
      </plugin>
      <!-- JaCoCo plugin -->
      <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <configuration>
          <excludes>
            <exclude>**/*Info.*</exclude>
          </excludes>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-report-plugin</artifactId>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <configuration>
          <links>
            <link>https://docs.oracle.com/javase/jp/8/docs/api/</link>
            <link>http://docs.spring.io/spring-boot/docs/1.5.1.RELEASE/api/</link>
          </links>
          <show>protected</show>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jxr-plugin</artifactId>
      </plugin>
    </plugins>
  </reporting>

  <name>BL.Cloud Java Application</name>
  <url>http://xxxxx.blcloud.jp/</url>
  <description>BL.Cloud Java アプリケーション</description>

  <organization>
    <name>Broadleaf Co.,Ltd.</name>
    <url>http://www.broadleaf.co.jp/</url>
  </organization>

  <distributionManagement>
    <site>
      <id>${project.artifactId}-site</id>
      <url>${project.baseUri}</url>
    </site>
    <repository>
      <id>central</id>
      <name>dev-repository-releases</name>
      <url>https://dev-repository.broadleaf.jp:8443/artifactory/bl_cloud</url>
    </repository>
    <snapshotRepository>
      <id>snapshots</id>
      <name>dev-repository-shapshot</name>
      <url>https://dev-repository.broadleaf.jp:8443/artifactory/bl_cloud</url>
    </snapshotRepository>
  </distributionManagement>
</project>